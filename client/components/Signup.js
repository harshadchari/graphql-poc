import React, { Component } from 'react'
import AuthForm from './AuthForm'
import mutation from './../mutations/Signup';
import { graphql } from 'react-apollo';
import query from './../queries/CurrentUser'
import { hashHistory } from 'react-router'

class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: []
        }
    }
    componentWillUpdate(nextProps, error) {
        /**
         * this.props - old set of props
         * nextProps - new set of props to be applied to the component
         */
        if (!this.props.data.user && nextProps.data.user) {
            //redirect to dashboard
            hashHistory.push('/dashboard');
        }

    }

    onSubmit({ email, password }) {
        this.props.mutate({
            variables: { email, password },
            refetchQueries: [{ query }]
        })
            .catch((res) => {
                const errors = res.graphQLErrors.map(error => error.message);
                this.setState({ errors });
            });
    }
    render() {
        return (
            <div>
                <h3>SignUp</h3>
                <AuthForm
                    errors={this.state.errors}
                    onSubmit={this.onSubmit.bind(this)} />
            </div>
        )
    }
}

export default graphql(query)(
    graphql(mutation)(Signup)
);
